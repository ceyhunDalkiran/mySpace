-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Gegenereerd op: 24 sep 2018 om 10:50
-- Serverversie: 10.1.21-MariaDB
-- PHP-versie: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myspace`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `likes`
--

CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `liked_user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(40) DEFAULT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `relationStatus` int(2) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `role`, `firstname`, `lastname`, `image`, `active`, `relationStatus`, `username`, `address`, `created_at`, `updated_at`) VALUES
(1, 'ads@ad.nl', '$2y$10$IhKsPZyNTqX/VQgNh.UAaORa5rTESVIm/P48uSxP7fNVOlfQAa3W2', 'user', 'asd', 'asdadadsdsa', 'standard.jpg', 1, 0, 'asddd', 'amsterdam', '2018-09-14 07:12:24', '2018-09-21 11:29:59'),
(2, 'justin@justin.com', '$2y$10$yqzCAcjPeLE0mqkdm/5O2.uw984haG.hB2pyK/dfelxOjXEzE5tuG', 'user', 'kees', 'willems', 'standard.jpg', 1, 2, 'justin556', 'mars', '2018-09-14 07:14:27', '2018-09-21 11:30:05'),
(3, 'irfan@bilir.nl', '$2y$10$kw6c17mmHbzibbHYCjHgFOx/2sxJ1aIeZhQJFFWVQM.2prfoXptTq', 'user', 'irfan', 'willieee', '78bf31eb8943be6f02055e2a7f651a0121860afb.jpg', 1, 3, 'irfan_willems', 'mars', '2018-09-14 07:42:10', '2018-09-21 09:17:48'),
(4, 'mikal@mikal.com', '$2y$10$n85bcObc5OAY2q4llQWEUumUb4Uo67lgFAJ9shzZu.6mtF7rGc0S.', 'user', 'Mikal', 'mikalooo', '78bf31eb8943be6f02055e2a7f651a0121860afb.jpg', 1, 0, 'mikal', 'Sun', '2018-09-21 07:07:42', NULL),
(5, 'mo@ali.com', '$2y$10$JmTSOVndj2S68m.ZtSEG2uTZShOJEuIib//OJEl6Gcs6IDL8mcQu6', 'user', 'Mo ', 'Willems', 'standard.jpg', 1, 0, 'Mo ', 'peterwillemstraat 2', '2018-09-21 07:41:09', '2018-09-21 11:30:10'),
(6, 'sofyan@bo.com', '$2y$10$MXj8C3tfrjXQDjJBrTUEzOJeWHNDNX.AGgeTPmeCt33b6Xqu4OIQa', 'user', 'Sofyan', 'kechistan', 'standard.jpg', 1, 3, 'sofyan', 'Mars', '2018-09-21 07:47:43', '2018-09-21 11:30:15');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
