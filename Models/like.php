<?php
class Like extends Model{

    protected $table = 'likes';

    /**
     * @Type int(11)
     */
    protected $user_id;

    /**
     * @Type int(11)
     */
    protected $liked_user_id;

    public static function increment()
    {
        // find like from user
        $query = DB::prepare("SELECT * FROM likes WHERE user_id = :user_id AND liked_user_id = :liked_user_id");
        $query->setFetchMode(PDO::FETCH_ASSOC);
        $query->execute([
            'user_id' => App::$user->id,
            'liked_user_id' => $_GET['liked_user_id'],
        ]);

        $query = $query->fetchAll();

        if(count($query) == 0){
            $like = new Like();
            $like->user_id = App::$user->id;
            $like->liked_user_id = $_GET['liked_user_id'];
            $like->save();
        }else
        {
            $deleteLike = DB::prepare("DELETE FROM likes WHERE user_id = :user_id AND liked_user_id = :liked_user_id ")->execute([
                'user_id' => App::$user->id,
                'liked_user_id' => $_GET['liked_user_id'],
            ]);
        }

    }

        protected static function newModel($obj)
        {
            return true;
        }

    }

?>
