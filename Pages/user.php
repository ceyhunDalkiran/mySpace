<?php
// App::pageAuth(["user"], "login");
$user = User::findBy('id', $_GET['id'])[0];
?>

<div class="container">
    <table class="table table-hover" >

        <?php if(App::checkAuth(App::ROLE_USER)){print_r("<tr><td><h4>Account of: " . $user->getFullName() . "</h4></th></tr>");} ?>
        <tr>
        <?php print_r("<td><h4>Username: " . $user->getUsername()) . "</h4></td>" ?>
        </tr>
        <tr>
        <?php if(App::checkAuth(App::ROLE_USER)){print_r("<td><h4>Email: " . $user->getEmail(). "</h4></td>");} ?>
        </tr>
        <tr>
        <?php if(App::checkAuth(App::ROLE_USER)){print_r("<td><h4>Relation status: " . $user->getRelationStatus()) . "</h4></td>" ;}?>
        </tr>
        <tr>
        <?php if(App::checkAuth(App::ROLE_USER)){print_r("<td><h4>Address: " . $user->getAddress() . "</h4></td>");} ?>
        </tr>
        <tr>
        <?php if(App::checkAuth(App::ROLE_USER)){print_r("<td><h4>Member since: " . $user->getCreatedAt() . "</h4></td>");} ?>
        </tr>
        <tr>
        <td><image class="round-circle" src="images/<?php  print_r($user->getImage()); if(empty($user->getImage())){ echo "standard.jpg";}?>"  ></image></td>
        </tr>
    </table>
    <button <?=App::link('likes&liked_user_id='.$user->id) ?> type='button' class='btn btn-outline-dark  ml-2' id='button'>LIKE</button>
</div>

<script>
    $('#button').click(function() {
    alert('you liked!');
    $.ajax({
     type: "GET",
     url: $(this).attr('href'),
    }).done(function() {
        increment();
    });

    });

</script>
